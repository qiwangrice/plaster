import os,sys,string,glob
from Bio import SeqIO
import time

#rnum: pan-genome size (number of plasmids)
rnum = 100

#repeat 20 experiments
for it in range(1,21):
    starttime = time.time()
    print("start",starttime)
    
    #random select a plasmid for update
    with open("/scratch/data/addgene/seqseqpan/random_plasmids_new_%i.txt"%it) as f1:
        for line in f1:
            entry = line.split("/")
            name = entry[-1]
            print(name)
        
            temp = name.split("_")[-1]
            INDEX = int(temp.split(".")[0])
            print(INDEX)
            filename = "addgene_length_plasmid_%i.fasta"%INDEX
            address = "/scratch/data/addgene_full_fasta/order_fasta_file/addgene_length_plasmid_%i.fasta"%INDEX

            # reference : pan-genome with size rnum (it: total 20 pan-genomes from 20 experiments)
        
            ref = "random_pangenome_%i_%i.fasta"%(rnum,it)
            print("ref",ref)

            #run dnadiff against current reference sequence
            os.system("/scratch/software/mummer-4.0.0beta/nucmer -t 80 --maxmatch %s %s -p %s.nucmer"%(ref,address,filename))
            deltaname = "addgene_length_plasmid_%i.fasta.nucmer.delta"%INDEX
            os.system("/scratch/software/mummer-4.0.0beta/dnadiff -d %s -p %s"%(deltaname,deltaname))

            #check if the fasta file is unalinged 
            if os.path.isfile("%s.nucmer.delta.report"%filename):

                # extract alignment percentage 
                with open("%s.nucmer.delta.report"%filename) as f1:
                    i = 0
                    for line in f1:
                        if i == 11:
                            #print(str(line))
                            entry = line.split()
                            alig = entry[2]
                            temp = alig.split('(')
                            percentage = temp[1][:-2]
                            print("percentage",str(percentage))
                            break
                        else:
                            i +=1

                #
                #begining writing 
                with open("pangenome_extend_%i_%i.fasta"%(rnum,it),"w") as handle:

                    # copy previous sequence to the new file
                    with open(ref) as f2:
                        for line in f2:
                            handle.write(line)

                    handle.write("NNNNNNNNNN")



                    #unalinged sequence 
                    #add whole partial sequence onto the reference sequence
                    if percentage == "0.00":

                        print("attach the whole sequence")
                    
                        #extract the whole sequence from file 
                        string = ""
                        with open(address) as f1:
                            next(f1)
                            for line in f1:
                                string = string + line[:-1]

                        #print("string",string)

                        start = 0
                        for N in range(0,100**100):

                            if start == 0:
                                end = start +70
                            else:
                                end = start + 80

                            if start in range(0,len(string)):
                                if end in range(0,len(string)):
                                    entry = string[start:end]
                                    #print(entry)
                                    handle.write(entry + "\n")
                                    start = end
                                else:
                                    entry = string[start:]
                                    #print(entry)
                                    handle.write(entry + "\n")
                                    break
                            else:
                                break

                    else:

                        # add part of sequence to the panref sequence 
                        if os.path.isfile("addgene_length_plasmid_%i.fasta.nucmer.delta.qdiff"%INDEX):

                            print("attach the broken sequence")

                            #write down attached fragments
                            record_dict = SeqIO.to_dict(SeqIO.parse(address, "fasta"))
                            with open("addgene_length_plasmid_%i.fasta.nucmer.delta.qdiff"%INDEX) as f2:

                                for line in f2:
 
                                    item = line.split('\t')
                                    id_number = str(item[0])
                                    strtype = str(item[1])
                                    start = int(item[2])
                                    end = int(item[3])
                                    length = end - start

                                    if strtype == "BRK" or strtype == 'GAP':
                                        if length > 50:
                                            sequence = str(record_dict[id_number].seq)
                                            output_sequence = sequence[start:end+1]
                                            #print(str(output_sequence))
                                            handle.write(output_sequence +"NNNNNNNNNN")


    endtime = time.time()
    print("end",endtime)
    totaltime = endtime - starttime

    with open("extend_time_%i_%i"%(rnum,it),"w") as timehandle:
        timehandle.write("random_"+str(rnum)+"_"+str(it)+"\t"+str(totaltime)+"\t"+str(starttime)+"\t"+str(endtime)+"\n")


