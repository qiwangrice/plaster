nat_syn_dict = {}
with open("nat_alignment_percentage_syn.text","r") as f1:
    for line in f1.readlines():
        if line[0] == "n":
            entry = line.split("\t")
            name = entry[0]
            percentage = entry[1]
        nat_syn_dict[name] = float(percentage)

nat_nat_dict = {}
with open("nat_alignment_percentage_nat.text","r") as f1:
    for line in f1.readlines():
        if line[0] == "n":
            entry = line.split("\t")
            name = entry[0]
            percentage = entry[1]
        nat_nat_dict[name] = float(percentage)
  
syn_syn_dict = {}
with open("syn_alignment_percentage_syn.text","r") as f1:
    for line in f1.readlines():
        if line[0] == "e":
            entry = line.split("\t")
            name = entry[0]
            percentage = entry[1]
        syn_syn_dict[name] = float(percentage)

syn_nat_dict = {}
with open("syn_alignment_percentage_nat.text","r") as f1:
    for line in f1.readlines():
        if line[0] == "e":
            entry = line.split("\t")
            name = entry[0]
            percentage = entry[1]
        syn_nat_dict[name] = float(percentage)
  

dict_nat = {}
for key in nat_syn_dict.keys():
    dict_nat[key] = (nat_syn_dict[key], nat_nat_dict[key])
     
dict_syn = {}
for key in syn_syn_dict.keys():
    dict_syn[key] = (syn_syn_dict[key], syn_nat_dict[key])
    
    
def classify_frags(dict, threshold):
    """
    Input: a dictionary where keys are plasmids' names and values are a list of percentage alignment; 
    a threshold list containing two threshold values 
    Output: a dictaionry where keys are plasmids' names and values are the plasmids' classified type
    """
    n_dict = {}
    for key in dict.keys():
        syn = dict[key][0]
        nat = dict[key][1]
        if syn >= threshold[0]:
            if nat >= threshold[1]:
                tag = "ambiguous sequence"
            else:
                tag = "synthetic sequence"
        else:
            if nat >= threshold[1]:
                tag = "natural sequence"
            else:
                tag = "low-alignment sequence"
        n_dict[key] = tag

    return n_dict

def get_values(dict1, dict2):
    """
    Input: two dictionaries where keys are the name of synthetic and natural plasmids and values 
    are their classified type (natural, synthetic, low-alignment, ambiguous)
    Output: generate four fasta_files containing the plasmid fragments of the four types
    """
    low_align = []
    syn_seq = []
    nat_seq = []
    amb_seq = []
    
    for key in dict1.keys():
        if dict1[key] == "low-alignment sequence":
            low_align.append(key)
        if dict1[key] == "natural sequence":
            nat_seq.append(key)
        if dict1[key] == "synthetic sequence":
            syn_seq.append(key)
        if dict1[key] == "ambiguous sequence":
            amb_seq.append(key)
            
    for key in dict2.keys():
        if dict2[key] == "low-alignment sequence":
            low_align.append(key)
        if dict2[key] == "natural sequence":
            nat_seq.append(key)
        if dict2[key] == "synthetic sequence":
            syn_seq.append(key)
        if dict2[key] == "ambiguous sequence":
            amb_seq.append(key) 
    nat_as_syn = 0
    syn_as_nat = 0
    syn_as_syn = 0
    nat_as_nat = 0
    
    amb_syn = 0
    amb_nat = 0
    low_syn = 0
    low_nat = 0
    # extra part
    for item in amb_seq:
        if item[0] == "n":
            amb_nat += 1
        if item[0] == "e":
            amb_syn +=1 
            
    for item in low_align:
        if item[0] == "n":
            low_nat += 1
        if item[0] == "e":
            low_syn +=1 
            
    # calculate interested values
    # 1. number of synthetic frags classified as synthetic 
    for item in syn_seq:
       if item[0] == "e":
           syn_as_syn += 1
           
    # 2. number of natural frags classified as synthetic 
    for item in syn_seq:
        if item[0] == "n":
            nat_as_syn += 1
            
    # 3. number of natural frags classified as natural 
    for item in nat_seq:
        if item[0] == "n":
            nat_as_nat += 1  
            
    # 4. number of synthetic frags classified as natural 
    for item in nat_seq:
       if item[0] == "e":
           syn_as_nat += 1
           
    # sensitivity for synthetic plasmid frags
    sen_syn = syn_as_syn/1000.0
    # specificity for synthetic plasmid frags
    spe_syn = (1000.0-nat_as_syn)/1000.0
    # sensitivity for natural plasmid frags
    sen_nat = nat_as_nat/1000.0
    # specificity for natural plasmid frags
    spe_nat = (1000.0-syn_as_nat)/1000.0

    return sen_syn, spe_syn, sen_nat, spe_nat
    #return amb_syn, amb_nat,low_syn, low_nat


# threshold_list1 = [(20,20),(20,40),(20,60),(20,80),(20,100)]
# threshold_list2 = [(40,20),(40,40),(40,60),(40,80),(40,100)]
# threshold_list3 = [(60,20),(60,40),(60,60),(60,80),(60,100)]
# threshold_list4 = [(80,20),(80,40),(80,60),(80,80),(80,100)]
# threshold_list5 = [(100,20),(100,40),(100,60),(100,80),(100,100)]
# threshold_list6 = [(80,80),(81,81),(82,82),(85,85),(90,90),(95,95),(97,97),(99,99)]

sen_syn_line = {}
spe_syn_line = {}
sen_nat_line = {}
spe_nat_line = {}

for item in threshold_list6:
    dic1 = classify_frags(dict_syn, item)
    dic2 = classify_frags(dict_nat, item)
    vals = get_values(dic1, dic2)
    sen_syn_line[item] = vals[0]
    spe_syn_line[item] = vals[1]
    sen_nat_line[item] = vals[2]
    spe_nat_line[item] = vals[3]

print  sen_syn_line
print  spe_syn_line
print  sen_nat_line
print  spe_nat_line