#plaster: build linear-based pan-genome quickly
#command line version
import os
import argparse
from Bio import SeqIO


def plaster(filelist, thread, attach_len, output_file_name):
    '''
        build pan-genome by input filelist.txt
        filelist.txt: input_file1 \t ifnput_file2 \t
    '''
    file1 = open(filelist)
    for line in file1:
        train_list = line.split("\t")
    train_list = list(filter(None, train_list))
    #sort files based on their sequence length
    train_file = {}
    for file in train_list:
        if "fasta" or "fa" in file:
            record = SeqIO.read("%s"%file, "fasta")
            train_file[file] = len(str(record.seq))

    #sort files based on seq len from long to short
    sorted_train_file = sorted(train_file.items(), key=lambda kv: kv[1], reverse=True)
    # initial file name
    init_file = sorted_train_file[0][0]
    #copy initial file to temporary pan-genome file
    os.system("scp %s temp_-1.fasta"%init_file)

    previous = -1

    #loop through all the input fasta file
    for index in range(1, len(sorted_train_file)):
        #reference file
        ref = "temp_%i.fasta"%previous
        filename = sorted_train_file[index][0]
        print(filename)
        #check if file exist in the folder
        if os.path.isfile(filename):
            os.system("./mummer-4.0.0beta2/nucmer -t %s --maxmatch %s %s -p %s.nucmer" \
                      %(thread, ref, filename, filename))
            deltaname = "%s.nucmer.delta"%filename
            os.system("./mummer-4.0.0beta2/dnadiff -d %s -p %s"%(deltaname, deltaname))
            if os.path.isfile("%s.report"%deltaname):
                with open("%s.report"%deltaname) as file2:
                    i = 0 
                    for line in file2:
                        if i == 11:
                            entry = line.split()
                            alig = entry[2]
                            temp = alig.split('(')
                            percentage = temp[1][:-2]
                            print("Aligned Bases (Query):", str(alig))
                            break
                        else:
                            i += 1
                #write new temparory pan-genome file
                with open("temp_%i.fasta"%index, "w") as handle:
                    with open("temp_%i.fasta"%previous) as file3:
                        for line in file3:
                            handle.write(line.rstrip().upper()+"\n")
                    handle.write("NNNNNNNNNN")
                    record = SeqIO.read("%s"%filename, "fasta")
                    string = str(record.seq).upper()
                    #unaligned sequence            
                    if percentage == "0.00":
                        print("attach the whole sequence")
                        #extract the whole sequence from file 
                        if len(string) > 70:
                            handle.write(string[:70]+"\n")
                            sub_string_list = [string[i:i+80] for i in range(70, len(string), 80)]
                            for sub in sub_string_list:
                                handle.write(sub+"\n")
                        else:
                            handle.write(string+"\n")
                    elif percentage == "100.00":
                        continue
                    else:
                        # add part of sequence to the panref sequence 
                        if os.path.isfile("%s.qdiff"%deltaname):
                            print("attach the broken sequence")
                            with open("%s.qdiff"%deltaname) as file4:
                                for line in file4:
                                    item = line.split('\t')
                                    strtype = str(item[1])
                                    start = int(item[2])
                                    end = int(item[3])
                                    length = end - start
                                    # only attach "BRK" and "GAP" fragments
                                    if strtype in ("BRK", 'GAP'):
                                        # only attach fragments w/ length longer than 50bp
                                        if length > attach_len:
                                            output_sequence = string[start+1:end]
                                            if len(output_sequence) > 70:
                                                handle.write(string[:70]+"\n")
                                                sub_string_list = [output_sequence[i:i+80] \
                                                                   for i in range(70, len(output_sequence), 80)]
                                                for sub in sub_string_list:
                                                    handle.write(sub+"\n")
                                            else:
                                                handle.write(output_sequence+"\n")
                                            handle.write("NNNNNNNNNN")
                    if index != (len(sorted_train_file)-1):
                        os.system("rm temp_%i.fasta"%previous)
                        os.system("rm %s"%deltaname)
                        os.system("rm %s.1coords %s.1delta %s.mcoords %s.mdelta %s.qdiff %s.rdiff %s.report %s.snps" \
                                  %(deltaname, deltaname, deltaname, deltaname, \
                                    deltaname, deltaname, deltaname, deltaname))
                    previous = index

        else:
            print("%s not included in the pan-genome"%filename)

    os.system("scp temp_%i.fasta %s.fasta"%(previous, output_file_name))
    os.system("rm temp_%i.fasta"%previous)
    os.system("rm %s.1coords %s.1delta %s.mcoords %s.mdelta %s.qdiff %s.rdiff %s.report %s.snps" \
              %(deltaname, deltaname, deltaname, deltaname, \
                deltaname, deltaname, deltaname, deltaname))

    if os.path.isfile("temp_-1.fasta"):
        os.system("rm temp_-1.fasta")



def main():
    '''
        main function
    '''
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("-i", "--input_file_name", \
                        help="a list of tab-separated input fasta file name")
    PARSER.add_argument("-o", "--output_file_name", default="output_pan_genome", \
                        help="output pan-genome fasta file name")
    PARSER.add_argument("-t", "--thread", default=20, \
                        help="Number of threads")
    PARSER.add_argument("-l", "--length", default=20, \
                        help="Minimum length of sequence attached to the pan-genome")
    OPTIONS = PARSER.parse_args()
    plaster(filelist=OPTIONS.input_file_name, thread=OPTIONS.thread, \
            attach_len=OPTIONS.length, output_file_name=OPTIONS.output_file_name)


#run program
main()
