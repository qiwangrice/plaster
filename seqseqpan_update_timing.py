import os,sys,string,glob
import time
# n: pangenome size
n = 100

# loop through 20 experiments
for it in range(1,21):

    starttime = time.time()
    print("start",starttime)
    os.system("seq-seq-pan-wga --config genomefile=random_plasmids_new_%i.txt outfilename=random_%i_%i_extended pangenome=random_%i_%i.xmfa"%(it,n,it,n,it))
    endtime = time.time()
    print("end",endtime)
    totaltime = endtime - starttime

    with open("seq-seq-pan_extend_timing_%i_%i.txt"%(n,it),"w") as handle:
        handle.write("random_"+str(n)+"_"+str(it)+"\t"+str(totaltime)+"\t"+str(starttime)+"\t"+str(endtime))
