import os,sys,string,glob
import time

with open("seq-seq-pan_myco_timing.txt","w") as handle:
    starttime = time.time()
    print("start",starttime)
    os.system("seq-seq-pan-wga --config genomefile=MycoT.txt outfilename=mycot")
    endtime = time.time()
    print("end",endtime)
    totaltime = endtime - starttime

    handle.write("seqpan_mycot"+"\t"+str(totaltime)+"\t"+str(starttime)+"\t"+str(endtime)+"\n")
