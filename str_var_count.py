import os
import subprocess
import numpy as np
from collections import defaultdict
strdic = {"BRK":0,"INV":0,"JMP":0,"GAP":0,"DUP":0,"SEQ":0}
strdic = defaultdict(int)
with open("addgene_str_var","w") as handle:
    for n in range(0,51057):
        if os.path.isfile("adp_full_length_%i.fasta.nucmer.delta.qdiff"%n):
            with open("adp_full_length_%i.fasta.nucmer.delta.qdiff"%n) as f1:
                print(n)
                for line in f1:
                    entry = line.split()
                    str_type = entry[1]
                    strdic[str_type]+=1
    
    for k,v in strdic.items():
        handle.write(str(k)+"\t"+str(v)+"\n")
                    
