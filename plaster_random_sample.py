import os,sys,string,glob
from Bio import SeqIO
import time
# rnum: number of plasmids used to build pan-genome
rnum = 10
for it in range(1,21):
    
    starttime = time.time()
    print("start",starttime)

    #extract seq from each fasta file for sorting 
    seqdic = {}
    with open("/scratch/software/seq-seq-pan/random_plasmids_%i_%i.txt"%(rnum,it)) as f1:
    
        for line in f1:
            entry = line.split("/")
            name = entry[-1]        
            temp = name.split("_")[-1]
            num = int(temp.split(".")[0])
            print(num)
        
            sort_filename = "addgene_length_plasmid_%i.fasta"%num
                
            #build dictionary based on sequence index {file index: seq}
            record_iter = SeqIO.parse(open(sort_filename),"fasta")
            for record in record_iter:
                seqdic[num] = record.seq            

    # sort plasmids based on length, output tuples (file id, pl sequence)
    sorted_record_iter = sorted(seqdic.items(), key=lambda kv: len(kv[1]),reverse = True) 

    #
    # copy the longest pl file as first reference : temp_-1.fasta
    checkfirst = 0 
    for item in sorted_record_iter:
    
        #file index 
        INDEX = item[0]
        filename = "addgene_length_plasmid_%i.fasta"%INDEX
    
        if checkfirst == 0:
        
            previous = -1
                
            ref = "temp_%s.fasta"%previous
        
            os.system("scp %s %s"%(filename,ref))
        
            checkfirst +=1
    
        else:
            break

    #
    # after sorting, create pan_ref seq
    #skip the longest pl : start from 1
    for spl in range(1,len(sorted_record_iter)):
    
        #file index 
        INDEX = sorted_record_iter[spl][0]
        filename = "addgene_length_plasmid_%i.fasta"%INDEX        
        ref = "temp_%s.fasta"%previous
        print("ref",ref)

        #run dnadiff against current reference sequence
        os.system("/scratch/software/mummer-4.0.0beta/nucmer --maxmatch %s %s -p %s.nucmer"%(ref,filename,filename))
        deltaname = "addgene_length_plasmid_%i.fasta.nucmer.delta"%INDEX
        os.system("/scratch/software/mummer-4.0.0beta/dnadiff -d %s -p %s"%(deltaname,deltaname))
   
        #check if the fasta file is unaligned
        #attach unaligned region 
        if os.path.isfile("addgene_length_plasmid_%i.fasta.nucmer.delta.report"%INDEX):

            # extract alignment percentage 
            with open("addgene_length_plasmid_%i.fasta.nucmer.delta.report"%INDEX) as f1:
                i = 0
                for line in f1:
                    if i == 11:
                        print(str(line))
                        entry = line.split()
                        alig = entry[2]
                        temp = alig.split('(')
                        percentage = temp[1][:-2]
                        print("percentage",str(percentage))
                        break
                    else:
                        i +=1

            #
            #begining writing 
            #new temp file
            with open("temp_%i.fasta"%INDEX,"w") as handle:

                # copy previous sequence to the new file
                with open("temp_%i.fasta"%previous) as f2:
                    for line in f2:
                        handle.write(line)

                #delimiter sequence
                handle.write("NNNNNNNNNN")



                #unalinged sequence 
                #add whole partial sequence onto the reference sequence
                if percentage == "0.00":

                    print("attach the whole sequence")
                    
                    #extract the whole sequence from file 
                    string = ""
                    with open(filename) as f1:
                        next(f1)
                        for line in f1:
                            string = string + line[:-1]

                    #print("string",string)

                    start = 0
                    for N in range(0,100**100):

                        if start == 0:
                            end = start +70
                        else:
                            end = start + 80

                        if start in range(0,len(string)):
                            if end in range(0,len(string)):
                                entry = string[start:end]
                                #print(entry)
                                handle.write(entry + "\n")
                                start = end
                            else:
                                entry = string[start:]
                                #print(entry)
                                handle.write(entry + "\n")
                                break
                        else:
                            break

                else:

                    # add part of sequence to the panref sequence 
                    if os.path.isfile("addgene_length_plasmid_%i.fasta.nucmer.delta.qdiff"%INDEX):

                        print("attach the broken sequence")

                        #write down attached fragments
                        record_dict = SeqIO.to_dict(SeqIO.parse(filename, "fasta"))
                        with open("addgene_length_plasmid_%i.fasta.nucmer.delta.qdiff"%INDEX) as f2:

                            for line in f2:

                                item = line.split('\t')
                                id_number = str(item[0])
                                strtype = str(item[1])
                                start = int(item[2])
                                end = int(item[3])
                                length = end - start

                                if strtype == "BRK" or strtype == 'GAP':
                                    if length > 50:
                                        sequence = str(record_dict[id_number].seq)
                                        output_sequence = sequence[start:end+1]
                                        #print(str(output_sequence))
                                        handle.write(output_sequence +"NNNNNNNNNN")

        previous = INDEX

    #timing the whole process 
    endtime = time.time()
    print("end",endtime)
    totaltime = endtime - starttime


    # write down run-time

    with open("random_time_%i_%i"%(rnum,it),"w") as timehandle:
        timehandle.write("random_"+str(rnum)+"_"+str(it)+"\t"+str(totaltime)+"\t"+str(starttime)+"\t"+str(endtime)+"\n")

#END
#after-process: move files into folder for next iteration
    
    os.system("mkdir random_%i_%i"%(rnum,it))
    os.system("mv *nucmer* random_%i_%i"%(rnum,it))
    os.system("mv temp_* random_%i_%i"%(rnum,it))
                                                                                                      
