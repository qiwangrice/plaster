#count the number of plasmids map to certain position in pan-genome sequence
import os
import subprocess
import numpy as np
with open("addgene_number_bp_alig","w") as handle:
    # the length of pan-genome sequence
    cover_list = np.zeros(18226201)
    
    #count
    for n in range(0,51057):
        if os.path.isfile("adp_full_length_%i.fasta.nucmer.delta.mcoords"%n):
            with open("adp_full_length_%i.fasta.nucmer.delta.mcoords"%n) as f1:
                print(n)
                for line in f1:
                    entry = line.split()
                    start = int(entry[0])
                    end = int(entry[1])
                    for m in range(start,end+1):
                        cover_list[m]+=1
                        print(str(cover_list[m]))
    
    for i in cover_list:
        handle.write(str(i)+"\n")
