import os,sys,string,glob
from multiprocessing import Pool

# perform pairwise alignment of plasimds against pan-genome sequence
#two steps:
#first, run nucmer to generate delta file . Input fasta files
#second, run dnadiff. Input delta file

def dnadiff(filename):
    #ref = "adp_full_partial_pan.fasta"
    ref = "npl_panref.fasta"
    
    #check if the file has created
    #step1
    #if os.path.isfile("%s.nucmer.delta"%filename):
    #step2
    if os.path.isfile("%s.dnadiff.snps"%filename):
    
        return True
    else:
        
        #step1
        #os.system("/scratch/software/mummer-4.0.0beta/nucmer --maxmatch %s %s -p %s.nucmer"%(ref,filename,filename))
        
        #step2
       os.system("/scratch/software/mummer-4.0.0beta/dnadiff -d %s -p %s"%(filename,filename))



#multiprocessing
pool = Pool(80)
list_filename = []
for i in range(0,51049):
    #step1 input file
    #filename = "adp_full_length_%i.fasta"%i
    #step2 input file
    filename = "adp_full_length_%i.fasta.nucmer.delta"%i
    list_filename.append(filename)

pool.map(dnadiff,list_filename)
    


