# extract alignment percentage from dnadiff report and qdiff file
import os
import subprocess
import numpy as np
#write down alignment percentage
with open("mycot_seqpan_order_alignment","w") as handle:
    # list of genomes in the data set
    with open("MycoT.txt") as f1:
        for line in f1:
            entry = line.split("/")
            name = entry[-1]
            print(name)
        
            filename = name[:-1]
            
            #extract total length & aligned base pairs in query sequence

            if os.path.isfile("%s.seqpan.delta.report"%filename): 
                with open("%s.seqpan.delta.report"%filename) as f1:
                    i = 0 
                    for line in f1:
                        if i == 11:
                            print(str(line))
                            entry = line.split()
                            alig = entry[2]
                            temp = alig.split('(')
                            alignedbase = int(temp[0])
                            break
                        
                        elif i ==10:
                            entry = line.split()
                            totalbase = int(entry[2])
                            i +=1

                        else:
                            i +=1
            
            #add invesion as aligned length

                if os.path.isfile("%s.seqpan.delta.qdiff"%filename):
                    with open("%s.seqpan.delta.qdiff"%filename) as f2:
                        for line in f2:
                            entry = line.split()
                            str_type = entry[1]
                            str_length = int(entry[4])
                            if str_length > 0:
                                if str_type == "INV":
                                    print(str_type,str_length)
                                    alignedbase += str_length


                # calculate total aligned percentage
                percentage = alignedbase/totalbase*100
                print(percentage)
                handle.write(str(filename)+"\t")
                handle.write(str(percentage)+"\t"+str(alignedbase)+"\n")

