import numpy as np
import math
entry = []
#with open("addgene_number_bp_alig") as f1:
#with open("nlp_number_bp_alig") as f1:
with open("addgene_number_bp_nlppanref_alig") as f1:
    for line in f1:
        item = float(line[:-1])
        entry.append(item)

entry = np.asarray(entry)
tablesize = list(range(1, len(entry)+1))

import matplotlib.pyplot as plt
# create stacked errorbars:
fig= plt.figure(figsize=(12,10))
plt.plot(tablesize,entry,'bo')
plt.xlabel("position",fontsize=18)
plt.ylabel('no.of plasmids', fontsize=18)
plt.tick_params(labelsize=18)

plt.savefig('/Users/qiwangrice/Downloads/WABI2019/adg_nplpanref_coverpl.png', dpi=300)
plt.show()